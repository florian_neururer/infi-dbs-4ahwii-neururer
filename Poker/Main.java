import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

	static final int NUMBERS = 13;
	static final int COLORS = 4;

	public static void main(String[] args) {

		Scanner s = new Scanner(System.in);
		System.out.println("How many attempts?");

		int attempts = s.nextInt();

		int[] results = new int[10];// 0. nichts 1. Paar 2. 2Paar 3.Triple
		// 4.Straight 5.Flush 6.FullHouse 7.Vier
		// 8.Straight Flush 9. Royal Flush

		int[] cards = new int[NUMBERS * COLORS];
		int[] hand = new int[5];

		for (int i = 0; i < cards.length; i++) {
			cards[i] = i;
		}

		long utx = System.currentTimeMillis();

		for (int t = 0; t < attempts; t++) {

			for (int i = 0; i < hand.length; i++) {

				int pos = (int) (Math.random() * (cards.length - i));
				hand[i] = cards[pos];
				cards[pos] = cards[cards.length - (i + 1)];
				cards[cards.length - (i + 1)] = hand[i];

			}

			if (isStraight(hand) && isFlush(hand) && value(hand[0]) == 8) {
				results[9]++;
			} else if (isFlush(hand) && isStraight(hand)) {
				results[8]++;
			} else if (isFlush(hand)) {
				results[5]++;
			} else if (isStraight(hand)) {
				results[4]++;
			} else {
				switch (pairs(hand)) {
				case 1: // pair
					results[1]++;
					break;

				case 2: // 2 pairs
					results[2]++;
					break;

				case 3: // triple
					results[3]++;
					break;

				case 6: // four
					results[7]++;
					break;

				case 4: // Full House
					results[6]++;
					break;

				default:
					break;
				}
			}
		}

		DecimalFormat df = new DecimalFormat("#.###");

		System.out.println(
				"1 Pair: " + results[1] + " which equals " + df.format(((double) results[1] / attempts) * 100) + "%");
		System.out.println(
				"2 Pairs: " + results[2] + " which equals " + df.format(((double) results[2] / attempts) * 100) + "%");
		System.out.println(
				"Triple: " + results[3] + " which equals " + df.format(((double) results[3] / attempts) * 100) + "%");
		System.out.println(
				"Straight: " + results[4] + " which equals " + df.format(((double) results[4] / attempts) * 100) + "%");
		System.out.println(
				"Flush: " + results[5] + " which equals " + df.format(((double) results[5] / attempts) * 100) + "%");
		System.out.println("Full House: " + results[6] + " which equals "
				+ df.format(((double) results[6] / attempts) * 100) + "%");
		System.out.println("Four of a kind: " + results[7] + " which equals "
				+ df.format(((double) results[7] / attempts) * 100) + "%");
		System.out.println("Straight Flush: " + results[8] + " which equals "
				+ df.format(((double) results[8] / attempts) * 100) + "%");
		System.out.println("Royal Flush: " + results[9] + " which equals "
				+ df.format(((double) results[9] / attempts) * 100) + "%");
		System.out.println();

		System.out.println(
				"Time required: " + df.format(((double) System.currentTimeMillis() - utx) / 1000) + " seconds");

		DBManager.getInstance().addAttempt(utx, attempts, results);
		DBManager.getInstance().close();
		s.close();

	}

	public static int value(int v) {
		return v % NUMBERS;

	}

	public static int color(int v) {
		return v / NUMBERS;

	}

	public static int pairs(int[] hand) {
		int pair = 0;

		for (int i = 0; i < hand.length - 1; i++) {
			for (int j = i + 1; j < hand.length; j++) {
				if (value(hand[i]) == value(hand[j])) {
					pair++;
				}
			}
		}

		return pair;
	}

	public static boolean isFlush(int[] hand) {

		for (int i = 0; i < hand.length - 1; i++) {
			if (color(hand[i]) != color(hand[i + 1])) {
				return false;
			}
		}

		return true;
	}

	public static boolean isStraight(int[] hand) {

		int[] newHand = new int[hand.length];
		for (int i = 0; i < newHand.length; i++) {
			newHand[i] = value(hand[i]);
		}
		Arrays.sort(newHand);

		for (int i = 0; i < newHand.length - 1; i++) {
			if (newHand[i + 1] != newHand[i] + 1) {
				return false;
			}
		}

		return true;

	}

}
