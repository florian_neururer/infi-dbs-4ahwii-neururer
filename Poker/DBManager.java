import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class DBManager {

    private Connection c;
    private static DBManager dbm;

    private DBManager() {
	try {
	    Class.forName("org.sqlite.JDBC");
	    c = DriverManager.getConnection("jdbc:sqlite:Poker.db");
	} catch (SQLException | ClassNotFoundException e) {
	    System.out.println("Connection to database failed!");
	}

    }

    public static DBManager getInstance() {

	if (dbm == null) {
	    dbm = new DBManager();
	    dbm.init();
	}

	return dbm;
    }

    private void init() {
	String sql = "CREATE TABLE IF NOT EXISTS results(UTS integer PRIMARY KEY,attempts integer NOT NULL,pairs integer NOT NULL,twoPairs integer NOT NULL,triples integer NOT NULL,straights integer NOT NULL,flushes integer NOT NULL,fullHouses integer NOT NULL,fourOfaKinds integer NOT NULL,straightFlushes integer NOT NULL,royalFlushes integer NOT NULL);";
	Statement stmt = null;
	try {
	    stmt = c.createStatement();
	    stmt.execute(sql);
	} catch (SQLException e) {

	    e.printStackTrace();
	} finally {
	    if (stmt != null) {
		try {
		    stmt.close();
		} catch (SQLException e) {
		    e.printStackTrace();
		}
	    }
	}

    }

    public void addAttempt(long utx, int attempts, int[] results) {

	PreparedStatement prestmt = null;

	String sql = "INSERT INTO results VALUES(?,?,?,?,?,?,?,?,?,?,?)";
	try {
	    prestmt = c.prepareStatement(sql);
	    prestmt.setLong(1, utx);
	    prestmt.setInt(2, attempts);
	    prestmt.setInt(3, results[1]);
	    prestmt.setInt(4, results[2]);
	    prestmt.setInt(5, results[3]);
	    prestmt.setInt(6, results[4]);
	    prestmt.setInt(7, results[5]);
	    prestmt.setInt(8, results[6]);
	    prestmt.setInt(9, results[7]);
	    prestmt.setInt(10, results[8]);
	    prestmt.setInt(11, results[9]);
	    prestmt.executeUpdate();
	} catch (SQLException e) {
	    e.printStackTrace();
	} finally {
	    if (prestmt != null) {
		try {
		    prestmt.close();
		} catch (SQLException e) {
		    e.printStackTrace();
		}
	    }
	}
    }

    public void close() {
	try {
	    c.close();
	} catch (SQLException e) {
	    e.printStackTrace();
	}
    }

}
