import static org.junit.Assert.*;

import org.junit.Test;

public class PokerTest {

    @Test
    public void testPairs() {
	int[] test = { 1, 2, 3, 4, 5 };
	assertEquals(0, Main.pairs(test));
	int[] test1 = { 1, 3, 16, 32, 23 };
	assertEquals(1, Main.pairs(test1));
	int[] test2 = { 3, 16, 29, 34, 22 };
	assertEquals(3, Main.pairs(test2));
	int[] test3 = { 4, 17, 30, 43, 23 };
	assertEquals(6, Main.pairs(test3));
	int[] test4 = { 4, 17, 29, 16, 23 };
	assertEquals(2, Main.pairs(test4));
	int[] test5 = { 4, 17, 30, 16, 29 };
	assertEquals(4, Main.pairs(test5));

    }

    @Test
    public void testIsFlush() {
	int[] test = { 1, 2, 3, 4, 5 };
	assertTrue(Main.isFlush(test));
	int[] test1 = { 1, 23, 34, 32, 3 };
	assertFalse(Main.isFlush(test1));
    }

    @Test
    public void testIsStraight() {

	int[] test = { 1, 2, 3, 4, 5 };
	assertTrue(Main.isStraight(test));
	int[] test1 = { 1, 2, 3, 4, 7 };
	assertFalse(Main.isStraight(test1));

    }

}
