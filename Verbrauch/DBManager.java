import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DBManager {

	private Connection c;
	private static DBManager dbm;
	private String tablename;
	private static String database;

	private DBManager() {
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:"+database);
		} catch (SQLException | ClassNotFoundException e) {
			System.out.println("Connection to database failed!");
			e.printStackTrace();
		}

	}

	public static DBManager getInstance() {

		if (dbm == null) {
			dbm = new DBManager();
		}

		return dbm;
	}

	public ArrayList<Consumption> getConsumptions() {
		ArrayList<Consumption> consumptions = new ArrayList<Consumption>();
		String sql = "SELECT * FROM " + tablename;
		PreparedStatement prestmt = null;
		ResultSet rs = null;

		try {
			prestmt = c.prepareStatement(sql);
			rs = prestmt.executeQuery();
			while (rs.next()) {
				consumptions.add(new Consumption(rs.getDate(1), rs.getDouble(2)));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (prestmt != null) {
				try {
					prestmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return consumptions;

	}

	public void insertConsumtion(Consumption consumption, long l) {

		PreparedStatement prestmt = null;
		try {

			String sql = "INSERT INTO " + tablename + " VALUES(?,?)";
			prestmt = c.prepareStatement(sql);
			prestmt.setLong(1, l);
			;
			prestmt.setDouble(2, consumption.getConsumtion());
			prestmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally

		{
			if (prestmt != null) {
				try {
					prestmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void close() {
		try {
			c.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void setTablename(String tablename){
		this.tablename=tablename;
	}
	
	public static void setDatabase(String databasename){
		database=databasename;
	}

}
