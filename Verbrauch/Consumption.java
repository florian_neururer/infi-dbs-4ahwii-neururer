import java.sql.Date;

public class Consumption {

	private Date date;
	private double consumtion;

	public Consumption(Date date, double consumtion) {

		this.date = new Date(date.getTime()*1000);
		this.consumtion = consumtion;
	}

	public Date getDate() {
		return date;
	}

	public double getConsumtion() {
		return consumtion;
	}

}
