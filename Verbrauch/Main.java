import java.io.File;
import java.sql.Date;
import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {

		ConfigManager config = new ConfigManager("config.txt");
		try {
			File f = new File("config.txt");
		} catch (Exception e) {
			config.writeConfig();
		}

		DBManager.setDatabase(config.getDatabase());
		DBManager.getInstance().setTablename(config.getTablename());

		ArrayList<Consumption> consumptions = DBManager.getInstance().getConsumptions();

		int previousMonth = consumptions.get(0).getDate().getMonth();
		int previousYear = consumptions.get(0).getDate().getYear();

		while (previousMonth != getMaxMonth(consumptions, getMaxYear(consumptions))
				|| previousYear != getMaxYear(consumptions)) {

			if (previousMonth != 11) {
				previousMonth++;
			} else {
				previousMonth = 0;
				previousYear++;
			}
			if (!contained(new Date(previousYear, previousMonth, 1), consumptions)) {
				Consumption c1 = getMin(consumptions, new Date(previousYear, previousMonth, 1), null);
				Consumption c2 = getMin(consumptions, new Date(previousYear, previousMonth, 1), c1);

				double con = getConsumption(c1, c2, new Date(previousYear, previousMonth, 1));

				DBManager.getInstance().getInstance().insertConsumtion(
						new Consumption(new Date(previousYear, previousMonth, 1), con),
						new Date(previousYear, previousMonth, 1).getTime() / 1000);
			}
		}

		for (Consumption consumption : getABS(DBManager.getInstance().getConsumptions())) {
			System.out.println(consumption.getDate() + " : " + consumption.getConsumtion());
		}

		System.out.println("Average: " + avg(getABS(DBManager.getInstance().getConsumptions())));
	}

	private static double avg(ArrayList<Consumption> c) {
		double sum = 0;
		for (Consumption consumption : c) {
			sum += consumption.getConsumtion();
		}
		return sum / c.size();
	}

	private static ArrayList<Consumption> getABS(ArrayList<Consumption> c) {

		ArrayList<Consumption> ABSonsumptions = new ArrayList<Consumption>();

		int previousMonth = c.get(0).getDate().getMonth();
		int previousYear = c.get(0).getDate().getYear();

		if (previousMonth != 11) {
			previousMonth++;
		} else {
			previousMonth = 0;
			previousYear++;
		}

		if (contained(new Date(previousYear, previousMonth, 1), c)) {
			Consumption c2 = getConsumption(c, new Date(previousYear, previousMonth, 1));
			ABSonsumptions.add(new Consumption(new Date(new Date(previousYear, previousMonth, 1).getTime() / 1000),
					c2.getConsumtion() - c.get(0).getConsumtion()));
		}

		while (previousMonth != getMaxMonth(c, getMaxYear(c)) || previousYear != getMaxYear(c)) {

			Consumption c1 = getConsumption(c, new Date(previousYear, previousMonth, 1));
			if (previousMonth != 11) {
				previousMonth++;
			} else {
				previousMonth = 0;
				previousYear++;
			}
			if (contained(new Date(previousYear, previousMonth, 1), c)) {
				Consumption c2 = getConsumption(c, new Date(previousYear, previousMonth, 1));

				ABSonsumptions.add(new Consumption(new Date(new Date(previousYear, previousMonth, 1).getTime() / 1000),
						c2.getConsumtion() - c1.getConsumtion()));
			}

		}

		return ABSonsumptions;
	}

	private static Consumption getConsumption(ArrayList<Consumption> c, Date d) {
		for (Consumption consumption : c) {
			if (consumption.getDate().equals(d)) {
				return consumption;
			}
		}
		return null;
	}

	private static boolean contained(Date date, ArrayList<Consumption> consumptions) {
		for (Consumption consumption : consumptions) {
			if (consumption.getDate().equals(date)) {
				return true;
			}
		}
		return false;
	}

	private static int getMaxMonth(ArrayList<Consumption> c, int year) {
		int i = 0;
		for (Consumption consumption : c) {
			if (consumption.getDate().getMonth() > i && consumption.getDate().getYear() == year) {
				i = consumption.getDate().getMonth();
			}
		}
		return i;

	}

	private static int getMaxYear(ArrayList<Consumption> c) {
		int i = 0;
		for (Consumption consumption : c) {
			if (consumption.getDate().getYear() > i) {
				i = consumption.getDate().getYear();
			}
		}
		return i;

	}

	private static Consumption getMin(ArrayList<Consumption> consumptions, Date date, Consumption exception) {

		Consumption c = null;
		long min = Math.abs(consumptions.get(consumptions.size() - 1).getDate().getTime()
				- consumptions.get(0).getDate().getTime());

		for (Consumption consumption : consumptions) {
			if (Math.abs(date.getTime() - consumption.getDate().getTime()) < min && exception == null) {
				c = consumption;
				min = Math.abs(date.getTime() - consumption.getDate().getTime());
			}
			if (exception != null) {

				if (!consumption.getDate().equals(exception.getDate())
						&& consumption.getConsumtion() != exception.getConsumtion()) {
					c = consumption;
					min = Math.abs(date.getTime() - consumption.getDate().getTime());
				}
			}
		}

		return c;

	}

	private static double getConsumption(Consumption c1, Consumption c2, Date first) {

		double slope = (c2.getConsumtion() - c1.getConsumtion()) / (c2.getDate().getTime() - c1.getDate().getTime());
		double intercept = c2.getConsumtion() - c2.getDate().getTime() * slope;
		return slope * first.getTime() + intercept;
	}
}
