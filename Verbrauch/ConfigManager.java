import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class ConfigManager {

	private String path;
	private Properties prop;

	public ConfigManager(String path) {
		this.path = path;
		FileInputStream in = null;
		try {
			in = new FileInputStream(path);
			prop = new Properties();
			prop.load(in);

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void writeConfig() {
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(path);
			Properties prop = new Properties();
			prop.setProperty("Datenbankpfad", "verbrauch.db");
			prop.setProperty("Tabellenname", "strom_verbrauch");
			prop.store(out, "---SQLite informationen---");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public String getDatabase() {

		return prop.getProperty("Datenbankpfad");
	}

	public String getTablename() {

		return prop.getProperty("Tabellenname");
	}

}
