
public class Customer {

	private int kundenID;
	private String name;
	private int sitzplatz;

	public Customer(int kundenID, String name, int sitzplatz) {
		super();
		this.kundenID = kundenID;
		this.name = name;
		this.sitzplatz = sitzplatz;
	}

	public Customer(String name, int sitzplatz) {
		this.name = name;
		this.sitzplatz = sitzplatz;
	}

	public int getKundenID() {
		return kundenID;
	}

	public void setKundenID(int kundenID) {
		this.kundenID = kundenID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSitzplatz() {
		return sitzplatz;
	}

	public void setSitzplatz(int sitzplatz) {
		this.sitzplatz = sitzplatz;
	}

}
