import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

	Scanner s = new Scanner(System.in);
	ConfigManager cm = new ConfigManager("config");
	DBManager dbm = new DBManager(cm.getSQLInformation());

	public Main() {

		boolean end = false;

		while (!end) {
			menu();
			try {
				switch (s.nextInt()) {
				case 1:
					addCustomer();
					break;
				case 2:
					addWagon();
					break;
				case 3:
					assignSeat();
					break;
				case 4:
					listNumber();
					break;
				case 5:
					listClassNames();
					break;
				case 6:
					listClassCount();
					break;
				case 7:
					listCustomers();
					break;
				case 8:
					listWagons();
					break;
				case 9:
					listAssignedSeats();
					break;
				case 10:
					deleteCustomer();
					break;
				case 11:
					deleteWagon();
					break;
				case 12:
					end = true;
					break;
				default:
					break;
				}
			} catch (InputMismatchException e) {
				s.next();
				System.err.println("Only insert a number!!!");
			}
			System.out.println();
		}

		dbm.close();
		s.close();
	}

	private void deleteWagon() {
		System.out.println("Which wagon do you want to delete?");
		dbm.deleteWagon(s.nextInt());
	}

	private void deleteCustomer() {
		System.out.println("Which customer do you want to delete?");
		dbm.deleteCustomer(s.nextInt());

	}

	private void listAssignedSeats() {

		for (ArrayList<String> a : dbm.getAssignedSeats()) {
			System.out.println(
					"ID: " + a.get(0) + "\tName:" + a.get(1) + "\tSeatnumber: " + a.get(2) + "\twagonID: " + a.get(3));
		}

	}

	public void listNumber() {
		System.out.println("There are " + dbm.getPeopleCount() + " people in the train");
	}

	public void listClassCount() {
		System.out.println("Of which class(1,2)?");
		System.out.println("There are " + dbm.getClassCount(s.nextInt()) + " people");
	}

	private void assignSeat() {
		System.out.println("Enter the seatnumber:");
		int seatnumber = s.nextInt();
		System.out.println("Enter the wagonID");
		int wagonID = s.nextInt();
		System.out.println("Enter customerID:");
		dbm.assignSeat(seatnumber, wagonID, s.nextInt());
	}

	private void listClassNames() {
		System.out.println("Of which class(1,2):");
		for (Customer c : dbm.getClassNames(s.nextInt())) {
			System.out
					.println("ID: " + c.getKundenID() + "\tName: " + c.getName() + "\tSeatnumber: " + c.getSitzplatz());
		}
	}

	private void addWagon() {
		System.out.println("What class is the wagon(1,2):");
		dbm.addWagon(new Wagon(s.nextInt()));
	}

	private void listCustomers() {
		for (Customer c : dbm.getCustomers()) {
			System.out
					.println("ID: " + c.getKundenID() + "\tName: " + c.getName() + "\tSeatnumber: " + c.getSitzplatz());
		}

	}

	private void listWagons() {
		for (Wagon w : dbm.getWagons()) {
			System.out.println("ID: " + w.getWagonID() + "\tClass: " + w.getKlasse());
		}

	}

	private void addCustomer() {

		System.out.println("Enter the name of the customer:");
		String name = s.next();
		String n = s.nextLine();
		System.out.println("Enter the seat number:");
		dbm.addCustomer(new Customer(name + n, s.nextInt()));
	}

	public static void main(String[] args) {
		new Main();
	}

	private void menu() {
		System.out.println("Choose option(enter number):");
		System.out.println("1: add customer\t\t2: add wagon\t\t3: assign seat");
		System.out.println("4: number of people\t5:Names in class\t6: Number of people in class");
		System.out.println("7: list costumers\t8: list wagons\t\t9: list assigned seats");
		System.out.println("10: delete cstomer \t11: delete wagon\t12:exit");
	}

}
