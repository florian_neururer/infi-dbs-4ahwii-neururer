
public class Wagon {

	private int wagonID;
	private int klasse;

	public Wagon(int wagonID, int klasse) {

		this.wagonID = wagonID;
		this.klasse = klasse;
	}

	public Wagon(int klasse) {

		this.klasse = klasse;
	}

	public int getWagonID() {
		return wagonID;
	}

	public void setWagonID(int wagonID) {
		this.wagonID = wagonID;
	}

	public int getKlasse() {
		return klasse;
	}

	public void setKlasse(int klasse) {
		this.klasse = klasse;
	}

}
