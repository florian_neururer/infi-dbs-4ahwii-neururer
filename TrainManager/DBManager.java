import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

public class DBManager {

	private Connection c;
	private Properties prop;

	public DBManager(Properties prop) {
		this.prop = prop;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + prop.getProperty("Datenbankpfad"));
		} catch (SQLException | ClassNotFoundException e) {
			System.err.println("Connection to database failed!");
		}

		init();
	}

	private void init() {
		Statement stmt = null;
		try {
			stmt = c.createStatement();

			String sql = "PRAGMA foreign_keys = ON";
			stmt.execute(sql);

			sql = "CREATE TABLE IF NOT EXISTS " + prop.getProperty("Kundentabelle") + " ("
					+ "kundenID INTEGER PRIMARY KEY AUTOINCREMENT," + "name TEXT NOT NULL,"
					+ "sitzplatz INTEGER NOT NULL)";
			stmt.execute(sql);

			sql = "CREATE TABLE IF NOT EXISTS " + prop.getProperty("Wagontabelle") + "("
					+ "wagonID INTEGER PRIMARY KEY AUTOINCREMENT," + "klasse INTEGER NOT NULL)";
			stmt.execute(sql);

			sql = "CREATE TABLE IF NOT EXISTS " + prop.getProperty("Buchungstabelle") + "("
					+ "wagonID INTEGER NOT NULL," + "sitzplatz INTEGER NOT NULL," + "kundenID INTEGER NOT NULL, "
					+ "FOREIGN KEY(kundenID) REFERENCES Kunde(kundenID),"
					+ "FOREIGN KEY (wagonID) REFERENCES Wagon(wagonID))";
			stmt.execute(sql);

		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("Failed to initialize database!");
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void addCustomer(Customer customer) {

		PreparedStatement prestmt = null;
		try {

			String sql = "INSERT INTO " + prop.getProperty("Kundentabelle") + "(name,sitzplatz) VALUES(?,?)";
			prestmt = c.prepareStatement(sql);
			prestmt.setString(1, customer.getName());
			prestmt.setInt(2, customer.getSitzplatz());
			prestmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			if (prestmt != null) {
				try {
					prestmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void addWagon(Wagon wagon) {

		PreparedStatement prestmt = null;
		try {

			String sql = "INSERT INTO " + prop.getProperty("Wagontabelle") + "(klasse) VALUES(?)";
			prestmt = c.prepareStatement(sql);
			prestmt.setInt(1, wagon.getKlasse());
			prestmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			if (prestmt != null) {
				try {
					prestmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void assignSeat(int seat, int wagon, int id) {

		PreparedStatement prestmt = null;
		try {

			String sql = "INSERT INTO " + prop.getProperty("Buchungstabelle") + " VALUES(?,?,?)";
			prestmt = c.prepareStatement(sql);
			prestmt.setInt(1, wagon);
			prestmt.setInt(2, seat);
			prestmt.setInt(3, id);
			prestmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			if (prestmt != null) {
				try {
					prestmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public ArrayList<ArrayList<String>> getAssignedSeats() {
		ArrayList<ArrayList<String>> assignedSeats = new ArrayList<ArrayList<String>>();
		String sql = "SELECT k.kundenID,k.NAME,k.sitzplatz, b.wagonID FROM " + prop.getProperty("Kundentabelle")
				+ " k, " + prop.getProperty("Buchungstabelle") + " b WHERE k.sitzplatz=b.sitzplatz";
		PreparedStatement prestmt = null;
		ResultSet rs = null;

		try {
			prestmt = c.prepareStatement(sql);
			rs = prestmt.executeQuery();
			while (rs.next()) {
				ArrayList<String> s = new ArrayList<String>();
				s.add(rs.getInt(1) + "");
				s.add(rs.getString(2));
				s.add(rs.getInt(3) + "");
				s.add(rs.getInt(4) + "");
				assignedSeats.add(s);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (prestmt != null) {
				try {
					prestmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return assignedSeats;

	}

	public int getPeopleCount() {
		String sql = "SELECT COUNT() FROM " + prop.getProperty("Buchungstabelle");
		PreparedStatement prestmt = null;
		ResultSet rs = null;
		int count = 0;

		try {
			prestmt = c.prepareStatement(sql);
			rs = prestmt.executeQuery();
			rs.next();
			count = rs.getInt(1);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (prestmt != null) {
				try {
					prestmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return count;

	}

	public ArrayList<Customer> getClassNames(int klasse) {
		ArrayList<Customer> customer = new ArrayList<Customer>();
		String sql = "SELECT k.kundenID,k.NAME,k.sitzplatz FROM " + prop.getProperty("Kundentabelle") + " k, "
				+ prop.getProperty("Buchungstabelle") + " b, " + prop.getProperty("Wagontabelle")
				+ " w WHERE k.sitzplatz=b.sitzplatz AND b.wagonID=w.wagonID AND w.klasse=?";
		PreparedStatement prestmt = null;
		ResultSet rs = null;

		try {
			prestmt = c.prepareStatement(sql);
			prestmt.setInt(1, klasse);
			rs = prestmt.executeQuery();
			while (rs.next()) {
				customer.add(new Customer(rs.getInt(1), rs.getString(2), rs.getInt(3)));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (prestmt != null) {
				try {
					prestmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return customer;

	}

	public int getClassCount(int klasse) {

		String sql = "SELECT COUNT() FROM " + prop.getProperty("Kundentabelle") + " k, "
				+ prop.getProperty("Buchungstabelle") + " b, " + prop.getProperty("Wagontabelle")
				+ " w WHERE k.sitzplatz=b.sitzplatz AND b.wagonID=w.wagonID AND w.klasse=?";
		PreparedStatement prestmt = null;
		ResultSet rs = null;
		int count = 0;

		try {
			prestmt = c.prepareStatement(sql);
			prestmt.setInt(1, klasse);
			rs = prestmt.executeQuery();

			rs.next();
			count = rs.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (prestmt != null) {
				try {
					prestmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return count;

	}

	public void close() {
		try {
			c.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public ArrayList<Customer> getCustomers() {
		ArrayList<Customer> customers = new ArrayList<Customer>();
		String sql = "SELECT * FROM " + prop.getProperty("Kundentabelle");
		PreparedStatement prestmt = null;
		ResultSet rs = null;

		try {
			prestmt = c.prepareStatement(sql);
			rs = prestmt.executeQuery();
			while (rs.next()) {
				customers.add(new Customer(rs.getInt(1), rs.getString(2), rs.getInt(3)));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (prestmt != null) {
				try {
					prestmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return customers;

	}

	public ArrayList<Wagon> getWagons() {
		ArrayList<Wagon> wagons = new ArrayList<Wagon>();
		String sql = "SELECT * FROM " + prop.getProperty("Wagontabelle");
		PreparedStatement prestmt = null;
		ResultSet rs = null;

		try {
			prestmt = c.prepareStatement(sql);
			rs = prestmt.executeQuery();
			while (rs.next()) {
				wagons.add(new Wagon(rs.getInt(1), rs.getInt(2)));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (prestmt != null) {
				try {
					prestmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return wagons;
	}

	public void deleteCustomer(int customerID) {

		PreparedStatement prestmt = null;
		try {

			String sql = "DELETE FROM " + prop.getProperty("Kundentabelle") + " WHERE kundenID=?";
			prestmt = c.prepareStatement(sql);
			prestmt.setInt(1, customerID);
			prestmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			if (prestmt != null) {
				try {
					prestmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public void deleteWagon(int wagonID) {
		PreparedStatement prestmt = null;
		try {

			String sql = "DELETE FROM " + prop.getProperty("Wagontabelle") + " WHERE wagonID=?";
			prestmt = c.prepareStatement(sql);
			prestmt.setInt(1, wagonID);
			prestmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			if (prestmt != null) {
				try {
					prestmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

}
