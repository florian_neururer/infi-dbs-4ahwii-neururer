import java.sql.Date;
import java.util.ArrayList;

public class Order {

    private int orderID;
    private int customerID;
    private Date orderDate;
    private double totalPrice;
    private ArrayList<OrderedArticle> orderedArticles;

    public Order(int orderID, int customer, Date orderDate, double totalPrice) {
	this.orderID = orderID;
	this.customerID = customer;
	this.orderDate = orderDate;
	this.totalPrice = totalPrice;
    }

    public Order(int customer, Date orderDate, double totalPrice) {
	this.orderID = -1;
	this.customerID = customer;
	this.orderDate = orderDate;
	this.totalPrice = totalPrice;
    }

    public ArrayList<OrderedArticle> getOrderedArticles() {
	if (orderedArticles == null) {
	    orderedArticles = DBManager.getInstance().getOrderedArticles(this);
	}
	return orderedArticles;
    }

    public void addOrderdArticle(OrderedArticle orderedArticle) {
	getOrderedArticles();
	orderedArticles.add(orderedArticle);
    }

    public void save() {
	DBManager.getInstance().save(this);
	if (orderedArticles != null
		)
	    for (OrderedArticle orderedArticle : orderedArticles) {
		orderedArticle.save();
	    }
    }

    public int getOrderID() {
	return orderID;
    }

    public void setOrderID(int orderID) {
	this.orderID = orderID;
    }

    public int getCustomer() {
	return customerID;
    }

    public void setCustomer(int customer) {
	this.customerID = customer;
    }

    public Date getOrderDate() {
	return orderDate;
    }

    public void setOrderDate(Date orderDate) {
	this.orderDate = orderDate;
    }

    public double getTotalPrice() {
	return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
	this.totalPrice = totalPrice;
    }

}
