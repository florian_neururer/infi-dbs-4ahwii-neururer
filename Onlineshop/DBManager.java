import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DBManager {

	private Connection c;
	private static DBManager dbm;

	private DBManager() {
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:Onlineshop.db");
		} catch (SQLException | ClassNotFoundException e) {
			System.out.println("Connection to database failed!");
			e.printStackTrace();
		}

	}

	public static DBManager getInstance() {

		if (dbm == null) {
			dbm = new DBManager();
			dbm.init();
		}

		return dbm;
	}

	private void init() {
		String sql = "CREATE TABLE IF NOT EXISTS Artikel(" + "Artikelnummer INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ "Artikelname varchar(50) NOT NULL," + "Artikelbeschreibung varchar(50) NOT NULL,"
				+ "Lagerbestand int NOT NULL," + "Preis Real NOT NULL" + "); ";

		String sql0 = "CREATE TABLE IF NOT EXISTS Kunde(" + "Kundennummer INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ "Vorname varchar(50) NOT NULL," + "Nachname varchar(50) NOT NULL," + "Geburtsdatum Date NOT NULL,"
				+ "Email varchar(50) NOT NULL," + "Benutzername varchar(50) NOT NULL,"
				+ "Passwort varchar(50) NOT NULL," + "IBAN varchar(50) NOT NULL," + "BIC varchar(50) NOT NULL,"
				+ "Hausnummer varchar(50) NOT NULL," + "Ort varchar(50) NOT NULL," + "Land varchar(50) NOT NULL,"
				+ "PLZ int NOT NULL" + ");";
		String sql2 = "CREATE TABLE IF NOT EXISTS BestellterArtikel(" + "Artikelnummer int NOT NULL,"
				+ "Bestellnummer int NOT NULL," + "Anzahl int NOT NULL,"
				+ "FOREIGN KEY (Artikelnummer) REFERENCES Artikel (Artikelnummer) ON DELETE CASCADE,"
				+ "FOREIGN KEY (Bestellnummer) REFERENCES Bestellung (Bestelnummer) ON DELETE CASCADE" + "); ";
		String sql1 = "CREATE TABLE IF NOT EXISTS Bestellung(" + "Bestelnummer INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ "Kundenummer int NOT NULL," + "Bestelldatum Date NOT NULL," + "Gesamtpreis int NOT NULL,"
				+ "FOREIGN KEY (Kundenummer) REFERENCES Kunde(Kundenummer) ON DELETE CASCADE" + "); ";
		Statement stmt = null;
		try {
			stmt = c.createStatement();
			stmt.execute(sql);
			stmt.execute(sql0);
			stmt.execute(sql1);
			stmt.execute(sql2);
		} catch (SQLException e) {

			e.printStackTrace();
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public ArrayList<Article> getArticles() {
		ArrayList<Article> articles = new ArrayList<Article>();
		String sql = "SELECT * FROM Artikel";
		PreparedStatement prestmt = null;
		ResultSet rs = null;

		try {
			prestmt = c.prepareStatement(sql);
			rs = prestmt.executeQuery();
			while (rs.next()) {
				articles.add(
						new Article(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4), rs.getDouble(5)));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (prestmt != null) {
				try {
					prestmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return articles;

	}

	public ArrayList<Customer> getCustomers() {
		ArrayList<Customer> customers = new ArrayList<Customer>();
		String sql = "SELECT * FROM Kunde";
		PreparedStatement prestmt = null;
		ResultSet rs = null;

		try {
			prestmt = c.prepareStatement(sql);
			rs = prestmt.executeQuery();
			while (rs.next()) {
				customers.add(new Customer(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getDate(4),
						rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9),
						rs.getString(10), rs.getString(11), rs.getString(12), rs.getString(13)));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (prestmt != null) {
				try {
					prestmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return customers;

	}

	public ArrayList<Order> getOrders(Customer customer) {
		ArrayList<Order> orders = new ArrayList<Order>();
		PreparedStatement prestmt = null;
		ResultSet rs = null;

		try {
			String sql = "SELECT * FROM Bestellung WHERE Kundenummer=?";
			prestmt = c.prepareStatement(sql);
			prestmt.setInt(1, customer.getCustomerID());
			rs = prestmt.executeQuery();
			while (rs.next()) {
				orders.add(new Order(rs.getInt(1), rs.getInt(2), rs.getDate(3), rs.getDouble(4)));
			}
		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			if (prestmt != null) {
				try {
					prestmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return orders;
	}

	public ArrayList<OrderedArticle> getOrderedArticles(Order order) {
		ArrayList<OrderedArticle> orderdArticles = new ArrayList<OrderedArticle>();
		PreparedStatement prestmt = null;
		ResultSet rs = null;

		try {
			String sql = "SELECT * FROM BestellterArtikel WHERE Bestellnummer=?";
			prestmt = c.prepareStatement(sql);
			prestmt.setInt(1, order.getOrderID());
			rs = prestmt.executeQuery();
			while (rs.next()) {
				Article a = null;
				ArrayList<Article> articles = new ArrayList<Article>();
				for (int i = 0; i < articles.size(); i++) {
					if (articles.get(i).getArticleID() == rs.getInt(2)) {
						a = articles.get(i);
						break;
					}
				}
				orderdArticles.add(new OrderedArticle(a, order, rs.getInt(3)));
			}
		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			if (prestmt != null) {
				try {
					prestmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return orderdArticles;
	}

	public void save(Article article) {
		PreparedStatement prestmt = null;
		ResultSet rs = null;
		try {
			if (article.getArticleID() != -1) {

				String sql = "UPDATE Artikel SET Artikelname=?, Artikelbeschreibung=?, Lagerbestand=?, Preis=? WHERE Artikelnummer=?";
				prestmt = c.prepareStatement(sql);
				prestmt.setString(1, article.getArticleName());
				prestmt.setString(2, article.getArticleDesc());
				prestmt.setInt(3, article.getStock());
				prestmt.setDouble(4, article.getPrice());
				prestmt.setInt(5, article.getArticleID());
				prestmt.executeUpdate();

			} else {
				String sql = "INSERT INTO Artikel(Artikelname,Artikelbeschreibung,Lagerbestand, Preis) VALUES(?,?,?,?)";
				prestmt = c.prepareStatement(sql);
				prestmt.setString(1, article.getArticleName());
				prestmt.setString(2, article.getArticleDesc());
				prestmt.setInt(3, article.getStock());
				prestmt.setDouble(4, article.getPrice());
				prestmt.executeUpdate();
				prestmt.close();
				sql = "SELECT last_insert_rowid()";
				prestmt = c.prepareStatement(sql);
				rs = prestmt.executeQuery();
				if (rs.next()) {
					article.setArticleID(rs.getInt(1));
				}

			}
		} catch (SQLException e) {
			e.printStackTrace();

		} finally

		{
			if (prestmt != null) {
				try {
					prestmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void save(Customer customer) {
		PreparedStatement prestmt = null;
		ResultSet rs = null;
		try {
			if (customer.getCustomerID() != -1) {

				String sql = "UPDATE Kunde SET Vorname=?, Nachname=?, Geburtsdatum=?, Email=?, Benutzername=?, Passwort=? ,IBAN=?, BIC=?, Hausnummer=? ,Ort=?, Land=?, PLZ=? WHERE Kundennummer=?";
				prestmt = c.prepareStatement(sql);
				prestmt.setString(1, customer.getFirstName());
				prestmt.setString(2, customer.getLastName());
				prestmt.setDate(3, customer.getBirthdate());
				prestmt.setString(4, customer.getEmail());
				prestmt.setString(5, customer.getUsername());
				prestmt.setString(6, customer.getPassword());
				prestmt.setString(7, customer.getIban());
				prestmt.setString(8, customer.getBic());
				prestmt.setString(9, customer.getHausnummer());
				prestmt.setString(10, customer.getCity());
				prestmt.setString(11, customer.getCountry());
				prestmt.setString(12, customer.getPLZ());
				prestmt.setInt(13, customer.getCustomerID());
				prestmt.executeUpdate();

			} else {
				String sql = "INSERT INTO Kunde(Vorname,Nachname,Geburtsdatum, Email,Benutzername,Passwort, IBAN, BIC, Hausnummer, Ort, Land, PLZ) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
				prestmt = c.prepareStatement(sql);
				prestmt.setString(1, customer.getFirstName());
				prestmt.setString(2, customer.getLastName());
				prestmt.setDate(3, customer.getBirthdate());
				prestmt.setString(4, customer.getEmail());
				prestmt.setString(5, customer.getUsername());
				prestmt.setString(6, customer.getPassword());
				prestmt.setString(7, customer.getIban());
				prestmt.setString(8, customer.getBic());
				prestmt.setString(9, customer.getHausnummer());
				prestmt.setString(10, customer.getCity());
				prestmt.setString(11, customer.getCountry());
				prestmt.setString(12, customer.getPLZ());
				prestmt.executeUpdate();
				prestmt.close();
				sql = "SELECT last_insert_rowid()";
				prestmt = c.prepareStatement(sql);
				rs = prestmt.executeQuery();
				if (rs.next()) {
					customer.setCustomerID(rs.getInt(1));
				}

			}
		} catch (SQLException e) {
			e.printStackTrace();

		} finally

		{
			if (prestmt != null) {
				try {
					prestmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void save(Order order) {
		PreparedStatement prestmt = null;
		ResultSet rs = null;
		try {
			if (order.getOrderID() != -1) {

				String sql = "UPDATE Bestellung SET Kundenummer=?, Bestelldatum=?, Gesamtpreis=? WHERE Bestelnummer=?";
				prestmt = c.prepareStatement(sql);
				prestmt.setInt(1, order.getCustomer());
				prestmt.setDate(2, order.getOrderDate());
				prestmt.setDouble(3, order.getTotalPrice());
				prestmt.setInt(4, order.getOrderID());
				prestmt.executeUpdate();

			} else {
				String sql = "INSERT INTO Bestellung(Kundenummer, Bestelldatum,Gesamtpreis) VALUES(?,?,?)";
				prestmt = c.prepareStatement(sql);
				prestmt.setInt(1, order.getCustomer());
				prestmt.setDate(2, order.getOrderDate());
				prestmt.setDouble(3, order.getTotalPrice());
				prestmt.executeUpdate();
				prestmt.close();
				sql = "SELECT last_insert_rowid()";
				prestmt = c.prepareStatement(sql);
				rs = prestmt.executeQuery();
				if (rs.next()) {
					order.setOrderID(rs.getInt(1));
				}

			}
		} catch (SQLException e) {
			e.printStackTrace();

		} finally

		{
			if (prestmt != null) {
				try {
					prestmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void save(OrderedArticle orderedArticle) {
		PreparedStatement prestmt = null;
		try {

			String sql = "INSERT INTO BestellterArtikel(Bestellnummer, Artikelnummer, Anzahl) VALUES(?,?,?)";
			prestmt = c.prepareStatement(sql);
			prestmt.setInt(1, orderedArticle.getOrder().getOrderID());
			prestmt.setInt(2, orderedArticle.getProduct().getArticleID());
			prestmt.setInt(3, orderedArticle.getNumber());
			prestmt.executeUpdate();
			prestmt.close();
			prestmt = c.prepareStatement(sql);

		} catch (SQLException e) {
			e.printStackTrace();

		} finally

		{
			if (prestmt != null) {
				try {
					prestmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}

	public void delete(Article article) {
		PreparedStatement prestmt = null;
		try {

			String sql = "DELETE FROM Artikel WHERE Artikelnummer = ?";
			prestmt = c.prepareStatement(sql);
			prestmt.setInt(1, article.getArticleID());
			prestmt.executeUpdate();
			prestmt.close();
			prestmt = c.prepareStatement(sql);

		} catch (SQLException e) {
			e.printStackTrace();

		} finally

		{
			if (prestmt != null) {
				try {
					prestmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}

	public void delete(Customer customer) {
		PreparedStatement prestmt = null;
		try {

			String sql = "DELETE FROM Kunde WHERE Kundennummer = ?";
			prestmt = c.prepareStatement(sql);
			prestmt.setInt(1, customer.getCustomerID());
			prestmt.executeUpdate();
			prestmt.close();
			prestmt = c.prepareStatement(sql);

		} catch (SQLException e) {
			e.printStackTrace();

		} finally

		{
			if (prestmt != null) {
				try {
					prestmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}

	public void close() {
		try {
			c.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
