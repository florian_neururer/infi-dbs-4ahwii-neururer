import java.sql.Date;
import java.util.ArrayList;

public class Customer {

	private int customerID;
	private String firstName;
	private String lastName;
	private Date birthdate;
	private String email;
	private String username;
	private String password;
	private String iban;
	private String bic;
	private String hausnummer;
	private String city;
	private String country;
	private String plz;
	private ArrayList<Order> orders;

	public Customer(int customerID, String firstName, String lastName, Date birthdate, String email, String username,
			String password, String iban, String bic, String hausnummer, String city, String country, String plz) {
		this.customerID = customerID;
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthdate = birthdate;
		this.email = email;
		this.username = username;
		this.password = password;
		this.iban = iban;
		this.bic = bic;
		this.hausnummer = hausnummer;
		this.city = city;
		this.country = country;
		this.plz = plz;
	}

	public Customer(String firstName, String lastName, Date birthdate, String email, String username, String password,
			String iban, String bic, String hausnummer, String city, String country, String plz) {
		this.customerID = -1;
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthdate = birthdate;
		this.email = email;
		this.username = username;
		this.password = password;
		this.iban = iban;
		this.bic = bic;
		this.hausnummer = hausnummer;
		this.city = city;
		this.country = country;
		this.plz = plz;
	}

	public ArrayList<Order> getOrder() {
		if (orders == null) {
			orders = DBManager.getInstance().getOrders(this);
		}
		return orders;
	}

	public void addOrder(Order order) {
		getOrder();
		orders.add(order);
	}

	public void delete() {
		DBManager.getInstance().delete(this);
	}

	public void save() {
		DBManager.getInstance().save(this);
		if (orders != null)
			for (Order order : orders) {
				order.save();
			}
	}

	public int getCustomerID() {
		return customerID;
	}

	public void setCustomerID(int customerID) {
		this.customerID = customerID;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public String getBic() {
		return bic;
	}

	public void setBic(String bic) {
		this.bic = bic;
	}

	public String getHausnummer() {
		return hausnummer;
	}

	public void setHausnummer(String hausnummer) {
		this.hausnummer = hausnummer;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPLZ() {
		return plz;
	}

	public void setPLZ(String plz) {
		this.plz = plz;
	}

}
