
public class OrderedArticle {

	private Article article;
	private Order order;
	private int number;

	public OrderedArticle(Article article, Order order, int number) {
		this.article = article;
		this.order = order;
		this.number = number;
	}

	public void save() {
		DBManager.getInstance().save(this);
	}

	public Article getProduct() {
		return article;
	}

	public void setProduct(Article product) {
		this.article = product;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

}
