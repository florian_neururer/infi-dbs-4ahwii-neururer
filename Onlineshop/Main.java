import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

	Scanner s = new Scanner(System.in);
	ArrayList<Article> articels = new ArrayList<Article>();
	ArrayList<Customer> customers = new ArrayList<Customer>();

	Customer c = null;

	public Main() {
		articels = DBManager.getInstance().getArticles();
		customers = DBManager.getInstance().getCustomers();
		boolean end = false;

		while (!end) {
			menu();
			switch (s.nextInt()) {
			case 1:
				addCustomer();
				break;
			case 2:
				addArticle();
				break;
			case 3:
				changeUser();
				break;
			case 4:
				buyArticle();
				break;
			case 5:
				viewOrders();
				break;
			case 6:
				deleteArticle();
				break;
			case 7:
				deleteCustomer();
				break;
			case 8:
				editCustomer();
				break;
			case 9:
				end = true;
				break;
			default:
				break;
			}
			System.out.println();

			for (Article article : articels) {
				article.save();
			}
			for (Customer customer : customers) {
				customer.save();
			}

		}
		DBManager.getInstance().close();
		s.close();
	}

	private void editCustomer() {

		if (c == null) {
			System.out.println("First choose a customer!");
		} else {
			System.out.println("Choose atribute to change!");
			System.out.println("1: first name\t2: last name\t3: password");
			System.out.println("4: IBAN\t5: BIC\t6: housenumber");
			System.out.println("7: city\t8: country\t9: zipcode");

			int number = s.nextInt();
			if (number < 1 || number > 9) {
				System.out.println("choose vaild number!");
			} else {
				System.out.println("Enter new value:");
				String st = s.next();
				switch (number) {
				case 1:
					c.setFirstName(st);
					break;
				case 2:
					c.setLastName(st);
					break;
				case 3:
					c.setPassword(st);
					break;
				case 4:
					c.setIban(st);
					break;
				case 5:
					c.setBic(st);
					break;
				case 6:
					c.setHausnummer(st);
					break;
				case 7:
					c.setCity(st);
					break;
				case 8:
					c.setCountry(st);
					break;
				case 9:
					c.setPLZ(st);
					break;
				}
			}

		}
	}

	private void viewOrders() {
		if (c == null) {
			System.out.println("First choose a customer!");
		} else {
			for (int i = 0; i < c.getOrder().size(); i++) {
				for (int j = 0; j < c.getOrder().get(i).getOrderedArticles().size(); j++) {
					System.out.println(
							c.getOrder().get(i).getOrderedArticles().get(j).getProduct().getArticleName() + "\t"
									+ c.getOrder().get(i).getOrderedArticles().get(j).getProduct().getPrice()
											* c.getOrder().get(i).getOrderedArticles().get(j).getNumber()
									+ "\t" + c.getOrder().get(i).getOrderedArticles().get(j).getNumber());
				}
			}
		}
	}

	private void buyArticle() {
		if (articels.size() == 0) {
			System.out.println("There are no atricles yet!");
		} else {
			if (c == null) {
				System.out.println("First choose a customer!");
			} else {
				Order o = new Order(c.getCustomerID(), new Date(System.currentTimeMillis()), 0);
				double price = 0;
				System.out.println("choose article");

				boolean end = false;
				while (!end) {
					for (int i = 0; i < articels.size(); i++) {
						System.out.println(i + ": \t" + articels.get(i).getArticleName() + "\t"
								+ articels.get(i).getPrice() + "€");
					}

					int number = s.nextInt();

					System.out.println("Select Amount");
					int amount = s.nextInt();
					if (articels.get(number).getStock() - amount > 0) {
						try {
							o.addOrderdArticle(new OrderedArticle(articels.get(number), o, amount));
							price += articels.get(number).getPrice();
							articels.get(number).setStock(articels.get(number).getStock() - amount);
						} catch (ArrayIndexOutOfBoundsException e) {
							System.out.println("choose a valid number!");
						}
					} else {
						System.out.println("Your requested amount is not in stock!");
					}
					System.out.println("buy more articles?(j/n)");
					String m = s.next();
					if (m.equals("n"))
						end = true;
				}

				o.setTotalPrice(price);
				c.addOrder(o);
			}
		}
	}

	private void changeUser() {
		if (customers.size() == 0) {
			System.out.println("There are no cusotmers yet!");
		} else {
			if (customers.size() == 0) {
				System.out.println("First you have to add a user!");
			} else {
				System.out.println("Select Customer");
				for (int i = 0; i < customers.size(); i++) {
					System.out.println(i + ": \t" + customers.get(i).getUsername());
				}
				int number = s.nextInt();
				try {
					c = customers.get(number);
				} catch (ArrayIndexOutOfBoundsException e) {
					System.out.println("choose valid number!");
				}
			}
		}
	}

	public static void main(String[] args) {
		new Main();
	}

	private void menu() {
		System.out.println("Choose option(enter number):");
		System.out.println("1: add customer\t2: add article\t3: choose customer");
		System.out.println("4: buy article\t5: view orders\t6: delete article");
		System.out.println("7: delete customer\t8: edit customer\t9: exit");
	}

	private void addCustomer() {
		System.out.println("Enter first name:");
		String fname = s.next();
		System.out.println("Enter last name");
		String lname = s.next();
		System.out.println("Enter Birthdate(dd-mm-jjjj)");
		String date = s.next();
		System.out.println("Enter email");
		String email = s.next();
		System.out.println("Enter username");
		String username = s.next();
		System.out.println("Enter password");
		String password = s.next();
		System.out.println("Enter IBAN");
		String iban = s.next();
		System.out.println("Enter BIC");
		String bic = s.next();
		System.out.println("Enter housenumber");
		String housenumber = s.next();
		System.out.println("Enter city");
		String city = s.next();
		System.out.println("Enter country");
		String country = s.next();
		System.out.println("Enter zipcode");
		String zipcode = s.next();

		SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy");
		formatter.setLenient(false);

		java.util.Date oldDate;
		try {
			oldDate = formatter.parse(date);
			long millis = oldDate.getTime();
			Date d = new Date(millis);

			customers.add(new Customer(fname, lname, d, email, username, password, iban, bic, housenumber, city,
					country, zipcode));
			c = customers.get(customers.size() - 1);
		} catch (ParseException e) {
			System.out.println("Wrong date format!");
		}

	}

	public void addArticle() {
		System.out.println("Enter Articlename:");
		String articlename = s.next();
		System.out.println("Enter articledescription");
		String articledesc = s.next();
		System.out.println("Enter Stock");
		int stock = s.nextInt();
		System.out.println("Enter price");
		double price = s.nextDouble();

		articels.add(new Article(articlename, articledesc, stock, price));

	}

	public void deleteArticle() {

		if (articels.size() == 0) {
			System.out.println("There are no atricles yet!");
		} else {

			System.out.println("choose article");

			for (int i = 0; i < articels.size(); i++) {
				System.out.println(
						i + ": \t" + articels.get(i).getArticleName() + "\t" + articels.get(i).getPrice() + "€");
			}

			int number = s.nextInt();

			try {

				articels.get(number).delete();
				articels.remove(number);

			} catch (ArrayIndexOutOfBoundsException e) {
				System.out.println("choose a valid number!");
			}
		}
	}

	public void deleteCustomer() {
		if (customers.size() == 0) {
			System.out.println("There are no customers yet!");
		} else {

			System.out.println("choose customer");

			for (int i = 0; i < customers.size(); i++) {
				System.out.println(i + ": \t" + customers.get(i).getUsername());
			}

			int number = s.nextInt();

			try {
				if (c.getCustomerID() == customers.get(number).getCustomerID()) {
					c = null;
				}
				customers.get(number).delete();
				customers.remove(number);

			} catch (ArrayIndexOutOfBoundsException e) {
				System.out.println("choose a valid number!");
			}

		}

	}
}
