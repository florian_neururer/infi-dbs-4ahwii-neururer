
public class Article {

	private int articleID;
	private String articleName;
	private String articleDesc;
	private int stock;
	private double price;

	public Article(int articleID, String articleName, String articleDesc, int stock, double price) {
		this.articleID = articleID;
		this.articleName = articleName;
		this.articleDesc = articleDesc;
		this.stock = stock;
		this.price = price;
	}

	public Article(String articleName, String articleDesc, int stock, double price) {
		this.articleID = -1;
		this.articleName = articleName;
		this.articleDesc = articleDesc;
		this.stock = stock;
		this.price = price;
	}

	public void save() {
		DBManager.getInstance().save(this);
	}

	public void delete() {
		DBManager.getInstance().delete(this);
	}

	public int getArticleID() {
		return articleID;
	}

	public void setArticleID(int articleID) {
		this.articleID = articleID;
	}

	public String getArticleName() {
		return articleName;
	}

	public void setArticleName(String articleName) {
		this.articleName = articleName;
	}

	public String getArticleDesc() {
		return articleDesc;
	}

	public void setArticleDesc(String articleDesc) {
		this.articleDesc = articleDesc;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

}
